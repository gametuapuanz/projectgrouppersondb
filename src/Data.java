
import java.util.ArrayList;
import java.util.Scanner;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author informatics
 */
public class Data {

    static ArrayList<Person> persons = new ArrayList<>();
    static Scanner kb = new Scanner(System.in);
    private static String username;
    private static String usernameEdit;
    private static String password;
    private static String passwordEdit;
    private static String firstname;
    private static String lastname;
    private static int age;
    private static String selectEdit;
    private static String newPassword;
    private static int numberUser;

    static void numberUser(int i) {
        numberUser = i;
    }

    static void load() {
        persons.add(new Person("1", "1", "name1", "surname1", 20));
        persons.add(new Person("test2", "password2", "name2", "surname2", 20));
        persons.add(new Person("test3", "password3", "name3", "surname3", 20));
        persons.add(new Person("test4", "password4", "name4", "surname4", 20));
        persons.add(new Person("test5", "password5", "name5", "surname5", 20));
    }

    static boolean isUserPasswordCorrect(String username, String password) {
        usernameEdit = username;
        if (username.equals("admin") && password.equals("admin")) {
            return true;
        }

        for (Person p : persons) {
            if (p.getUsername().equals(username)) {
                if (p.getPassword().equals(password)) {
                    return true;
                }
            }
        }
        return false;
    }

    static void add() {
        inputUsername();
        System.out.print("Password:");
        password = kb.next();
        System.out.print("Firstname:");
        firstname = kb.next();
        System.out.print("Lastname:");
        lastname = kb.next();
//        while (true) {
        System.out.print("Age:");
        age = kb.nextInt();
//            CheckAge(age);
//            break;
//        }
        persons.add(new Person(username, password, firstname, lastname, age));
        PersonDB.menu();
    }

    private static void inputUsername() {
        while (true) {
            System.out.print("Username:");
            username = kb.next();
            if (CheckUsername(username)) {
                break;
            }
        }
    }

    private static boolean CheckUsername(String username) {
        for (Person p : persons) {
            if (p.getUsername().equals(username)) {
                System.out.println("Username Do not repeat.");
                return false;
            }
        }
        return true;
    }

    private static boolean CheckAge(int age) {
        return true;
    }

    private static void inputPasswordEdit() {
        System.out.println(usernameEdit);
        while (true) {
            System.out.print("Password:");
            passwordEdit = kb.nextLine();
            if (CheckPassword(usernameEdit, passwordEdit)) {
                break;
            }
        }
    }

    private static boolean CheckPassword(String username, String password) {
        for (Person p : persons) {
            if (p.getUsername().equals(username)) {
                if (p.getPassword().equals(password)) {
                    return true;
                }
            }
        }
        System.out.println("Password is not correct!!");
        return false;
    }

    static void edit() {
        inputPasswordEdit();
        menuEdit();
    }

    private static void menuEdit() {
        System.out.println(numberUser);
        System.out.println("1.Username");
        System.out.println("2.Password");
        System.out.println("3.Firstname");
        System.out.println("4.Lastname");
        System.out.println("5.Age");
        System.out.println("0.Back");
        System.out.print("Please Select number:");
        selectEdit = kb.next();
        if (selectEdit.equals("1")) {
            EditUsername();
        } else if (selectEdit.equals("2")) {
            EditPassword();
        } else if (selectEdit.equals("3")) {
            EditFirstname();
        } else if (selectEdit.equals("4")) {
            EditLastname();
        } else if (selectEdit.equals("5")) {
            EditAge();
        } else if (selectEdit.equals("0")) {
            PersonDB.menu();
        } else {
            menuEdit();
        }
    }

    static void Seelist() {
        for (int i = 0; i < persons.size(); i++) {
            System.out.println(i + 1 + " " + persons.get(i).getFristname());
            if (i == persons.size() - 1) {
                System.out.println((persons.size() + 1) + " Back to Menu");
            }
        }
        System.out.print("Please select menu:");
        int select = kb.nextInt() - 1;
        if (select == persons.size()) {
            PersonDB.menu();
        } else if (select > -1 && select < persons.size()) {
            persons.get(select).showData();
        } else {
            Seelist();
        }

        System.out.print("Press B Back to Seelist:");
        char press = kb.next().charAt(0);
        if (press == 'B' || press == 'b') {
            Seelist();
        }
    }

    private static void EditPassword() {
        System.out.println("Old password:" + passwordEdit);
        System.out.print("New password:");
        newPassword = kb.next();

        for (Person p : persons) {
            if (p.getPassword().equals(passwordEdit)) {
                p.setPassword(newPassword);
                menuEdit();
            }
        }
    }

    private static void EditFirstname() {
        System.out.println("Old firstname:" + persons.get(numberUser).getFristname());
        System.out.print("New firstname:");
        String newFirstname = kb.next();

        persons.get(numberUser).setFristname(newFirstname);
        menuEdit();

    }

    private static void EditLastname() {
        System.out.println("Old lasttname:" + persons.get(numberUser).getLastname());
        System.out.print("New lasttname:");
        String newLastname = kb.next();

        persons.get(numberUser).setLastname(newLastname);
        menuEdit();
    }

    private static void EditAge() {
        System.out.println("Old Age:" + persons.get(numberUser).getAge());
        System.out.print("New Age:");
        int newAge = kb.nextInt();
        
        persons.get(numberUser).setAge(newAge);
        menuEdit();
    }

    private static void EditUsername() {

        System.out.println("Old lasttname:" + persons.get(numberUser).getUsername());
        String newUsername = "";

        while (true) {
            System.out.print("New lasttname:");
            newUsername = kb.next();
            if (CheckUsername(newUsername)) {
                break;
            }
        }

        persons.get(numberUser).setUsername(newUsername);
        menuEdit();
    }

}

//    private static void EditUsername() {
//        System.out.println("Old username:" + usernameEdit);
//        System.out.println("New username:");
//
//        for (Person p : persons) {
//            if (p.getUsername().equals(usernameEdit)) {
//                //persons.ad
//            }
//        }
//
//    }

