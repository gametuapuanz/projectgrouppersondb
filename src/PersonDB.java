/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.Scanner;

/**
 *
 * @author informatics
 */
public class PersonDB {
    
    private static String username;
    private static String password;
    static Scanner kb = new Scanner(System.in);
    private static String select;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        login();
    }
    
    private static void inputLogin() {
        System.out.println("Login");
        System.out.print("Username:");
        username = kb.next();
//        username = "test1";
        System.out.print("Password:");
        password = kb.next();
//        password = "pass1";
        checkNumberUser();
    }
    
    private static void showLoginError() {
        System.out.println("Username or Password is incorrected!!!");
        System.out.println("Try agian.");
    }
    
    private static void login() {
        Data.load();
        while (true) {
            inputLogin();
            
            if (!Data.isUserPasswordCorrect(username, password)) {
                showLoginError();
                continue;
            }
            break;
        }
        menu();
    }
    
    static void menu() {
        System.out.println("1.Add");
        System.out.println("2.Edit");
        System.out.println("3.Seelist");
        System.out.println("4.Logout");
        System.out.print("Please Select number:");
        select = kb.next();
        if (select.equals("1")) {
            Data.add();
        } else if (select.equals("2")) {
            Data.edit();
        } else if (select.equals("3")) {
            Data.Seelist();
        }else if (select.equals("4")) {
            login();
        } else {
            ErrorSelectMenu();
        }        
    }
    
    private static void ErrorSelectMenu() {
        System.out.println("Select menu must be number (1-3)!!");
        menu();
    }
    
    private static void checkNumberUser() {
        for (int i = 0; i < Data.persons.size(); i++) {
            if (Data.persons.get(i).getUsername().equals(username)) {
                Data.numberUser(i);
            }
        }
        
    }
    
}
